
IMG ?= fyl253711/kubehark-auth:v13
SHELL := /bin/bash

help: ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


fmt: ## Run go fmt against code.
	go fmt ./...

vet: ## Run go vet against code.
	go vet ./...

build:  ## Build manager binary.
	go build -o bin/manager main.go

build-linux:  ## Build manager binary.
	go mod tidy && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/manager main.go

docker-build: build-linux  ## Build docker image with the manager.
	docker build -t ${IMG} .

docker-push: docker-build ## Build docker image with the manager.
	docker push  ${IMG}

run-dev: ## Build docker image with the manager.
	go  run main.go server --kubeconfig=/Users/loganfang/go/src/gitee/kubehark/kubehark-auth/bin/k8sconfig_dev --rego-model-path=/Users/loganfang/go/src/gitee/kubehark/kubehark-auth/bin/example.rego --rego-model-path=/Users/loganfang/go/src/gitee/kubehark/kubehark-auth/bin/example_data.rego