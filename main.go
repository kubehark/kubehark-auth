package main

import (
	"fmt"
	"gitee/kubehark/kubehark-auth/cmd"
	"os"
)

func main()  {
	cmd := cmd.NewRootCmd(os.Args[1:])
	if err := cmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
