package model

type Rolegrant_model struct {
	Name   string   `json:"name"`
	Method []string `json:"method"`
	Labels map[string]string `json:"labels"`
}
