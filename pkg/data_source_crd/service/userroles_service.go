package service

import (
	"context"
	"errors"
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd/model"
	v1 "gitee/kubehark/kubehark-operator/api/kubehark/v1"
	"gitee/kubehark/kubehark-operator/client/clientset/versioned"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Userroles_service struct {
	Client *versioned.Clientset
}

func (this *Userroles_service) Post(model model.Userroles_model) error {
	if model.UserName == "" {
		return errors.New("name 为必填项目")
	}
	CreateModel := v1.UserRol{
		TypeMeta: metav1.TypeMeta{
			APIVersion: v1.GroupVersion.Group,
			Kind:       v1.GroupVersion.Group,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      model.UserName,
			Namespace: "default",
		},
		Spec: v1.UserRolSpec{
			UserName:  model.UserName,
			Roles:     model.Roles,
			WhiteList: model.WhiteList,
		},
	}
	_, err := this.Client.KubeharkV1().UserRols("default").Create(context.TODO(), &CreateModel, metav1.CreateOptions{})
	return err
}

func (this *Userroles_service) Delete(model model.Userroles_model) error {
	if model.UserName == "" {
		return errors.New("name 为必填项目")
	}
	return this.Client.KubeharkV1().UserRols("default").Delete(context.TODO(), model.UserName, metav1.DeleteOptions{})
}

func (this *Userroles_service) Put(model model.Userroles_model) error {
	if model.UserName == "" {
		return errors.New("name 为必填项目")
	}

	data, err := this.Client.KubeharkV1().UserRols("default").Get(context.TODO(), model.UserName, metav1.GetOptions{})
	if err != nil {
		return err
	}

	CreateModel := v1.UserRol{
		TypeMeta: metav1.TypeMeta{
			APIVersion: v1.GroupVersion.Group,
			Kind:       v1.GroupVersion.Group,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:            model.UserName,
			Namespace:       "default",
			ResourceVersion: data.ObjectMeta.ResourceVersion,
		},
		Spec: v1.UserRolSpec{
			UserName:  model.UserName,
			Roles:     model.Roles,
			WhiteList: model.WhiteList,
		},
	}
	_, err = this.Client.KubeharkV1().UserRols("default").Update(context.TODO(), &CreateModel, metav1.UpdateOptions{})
	return err
}

func (this *Userroles_service) Get(model model.Userroles_model) (*model.Userroles_model, error) {
	if model.UserName == "" {
		return nil, errors.New("name 为必填项目")
	}
	data, err := this.Client.KubeharkV1().UserRols("default").Get(context.TODO(), model.UserName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	model.UserName = data.Spec.UserName
	model.Roles = data.Spec.Roles
	model.WhiteList = data.Spec.WhiteList
	return &model, nil
}

func (this *Userroles_service) GetList(mo model.Userroles_model) ([]*model.Userroles_model, error) {

	data, err := this.Client.KubeharkV1().UserRols("default").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	returndata := []*model.Userroles_model{}
	for _, v := range data.Items {
		da := model.Userroles_model{}

		da.Roles = v.Spec.Roles
		da.WhiteList = v.Spec.WhiteList
		da.UserName = v.Spec.UserName
		returndata = append(returndata, &da)
	}
	return returndata, nil
}
