package data_source_crd

import (
	"fmt"
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd/model"
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd/service"
	"testing"
)

var kubeconfigstring = `
apiVersion: v1
clusters:
- cluster:
    insecure-skip-tls-verify: true
    server: https://106.12.59.10:6443
  name: hark-1
contexts:
- context:
    cluster: hark-1
    user: admin
  name: master
current-context: master
kind: Config
preferences: {}
users:
- name: admin
  user:
    username: admin
    password: admin
`

// TestCmd comment lint rebel
func TestCmd(t *testing.T) {
	Init("/Users/yulongfang/go/src/gitee/kubehark/kubehark-operator/k8sconfig_dev")
	Credential_service := service.Credential_service{
		Client: ClientCrd,
	}
	cd := model.Credential_model{
		Name:       "cred",
		Kubeconfig: kubeconfigstring,
		Labels: map[string]string{
			"A": "A",
			"B": "B",
			"c": "c2",
		},
	}
	//fmt.Println(Credential_service.Post(cd))
	//fmt.Println("执行成功")

	fmt.Println(Credential_service.Delete(cd))
}
