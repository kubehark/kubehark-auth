package opa

import (
	"fmt"
	"reflect"
	"testing"
)

func TestCmd(t *testing.T) {

	labels := map[string]string{
		"A":"A",
	}

	files := []string{
		"/Users/loganfang/go/src/gitee/kubehark/kubehark-framwork/pkg/Opa/example.rego",
		"/Users/loganfang/go/src/gitee/kubehark/kubehark-framwork/pkg/Opa/example_data.rego",
	}


	result ,err:=Opa(labels,"GET","test_user",&files)
	fmt.Println(err)
	fmt.Println(result)
	fmt.Println(reflect.TypeOf(result))
	//allow, ok := result[0].Bindings["allow"].(bool)
	//fmt.Println(allow,ok)
}
