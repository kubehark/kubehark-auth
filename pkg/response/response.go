package response

import (
	"github.com/gin-gonic/gin"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
)


const (
	SUCCESS int = 200 //操作成功
	FAILED  int = 1   // 操作失败
)
func OpaSuccess(c *gin.Context, code int, message string) {
	c.JSON(code, metav1.Status{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "v1",
			Kind:       "Status",
		},
		Code:    http.StatusOK,
		Status:  metav1.StatusSuccess,
		Message: message,
	})
}

func OpaFail(c *gin.Context, code int, message string) {
	c.JSON(code, metav1.Status{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "v1",
			Kind:       "Status",
		},
		Code:    http.StatusUnauthorized,
		Status:  metav1.StatusFailure,
		Message: message,
	})
}

// 普通返回成功
func GinSuccess(ctx *gin.Context, v interface{}) {
	ctx.JSON(http.StatusOK, map[string]interface{}{
		"code": SUCCESS,
		"smg":  "成功",
		"data": v,
	})
}



//普通的操作失败返回
func GInFailed(ctx *gin.Context, v interface{}) {
	ctx.JSON(http.StatusOK, map[string]interface{}{
		"code": FAILED,
		"msg":  v,
	})
}
