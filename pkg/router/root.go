package router

import (
	v1 "gitee/kubehark/kubehark-auth/pkg/router/v1"
	"github.com/gin-gonic/gin"
)

func InitRootRouter(regoPath []string) *gin.Engine {
	root := gin.Default()
	v1.V1(root, regoPath)

	return root
}
