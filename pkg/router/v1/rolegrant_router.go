package v1

import (
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd"
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd/model"
	"gitee/kubehark/kubehark-auth/pkg/response"
	"github.com/gin-gonic/gin"
)

// get
//   获取单个的 RoleGrant
func GetOneRoleGrant(c *gin.Context) {
	var RolegrantlQuery model.Rolegrant_model
	RolegrantlQuery.Name = c.Query("name")
	if RolegrantlQuery.Name =="" {
		response.GInFailed(c, "name不能为空")
		return
	}
	data, err := data_source_crd.Rolegrant_service.Get(RolegrantlQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, data)
}

// getlist
// 获取RoleGrant 列表
func GetListRoleGrant(c *gin.Context) {
	var RoleGrantQuery model.Rolegrant_model
	//err := c.BindJSON(&RoleGrantQuery)
	//if err != nil {
	//	response.GInFailed(c, "输入参数不正确")
	//}
	data, err := data_source_crd.Rolegrant_service.GetList(RoleGrantQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, data)
}

// post
// 创建一个 RoleGrant
func CreateRoleGrant(c *gin.Context) {
	var RoleGrantQuery model.Rolegrant_model
	err := c.BindJSON(&RoleGrantQuery)
	if err != nil {
		response.GInFailed(c, "输入参数不正确")
	}
	err = data_source_crd.Rolegrant_service.Post(RoleGrantQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "创建成功")
}

// put
// 更新 RoleGrant
func PutListRoleGrant(c *gin.Context) {
	var RoleGrantQuery model.Rolegrant_model
	err := c.BindJSON(&RoleGrantQuery)
	if err != nil {
		response.GInFailed(c, "输入参数不正确")
	}
	err = data_source_crd.Rolegrant_service.Put(RoleGrantQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "修改成功")
}

// delete
// 删除 RoleGrant
func DeleteListRoleGrant(c *gin.Context) {
	var RolegrantlQuery model.Rolegrant_model
	RolegrantlQuery.Name = c.Query("name")
	if RolegrantlQuery.Name =="" {
		response.GInFailed(c, "name不能为空")
		return
	}
	err := data_source_crd.Rolegrant_service.Delete(RolegrantlQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "删除成功")
}
