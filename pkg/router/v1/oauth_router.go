package v1

import (
	"gitee/kubehark/kubehark-auth/pkg/opa"
	"gitee/kubehark/kubehark-auth/pkg/response"
	"github.com/gin-gonic/gin"
)



// get
//   获取单个的 Userroles_model
func OauthResult(c *gin.Context) {

	var OpaInput opa.Input
	err := c.BindJSON(&OpaInput)
	if err != nil {
		response.GInFailed(c, err)
		return
	}
	result, err := opa.Opa(OpaInput.Labels, OpaInput.Method, OpaInput.User, &RegoModelPath)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, result)
}
