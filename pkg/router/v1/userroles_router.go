package v1

import (
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd"
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd/model"
	"gitee/kubehark/kubehark-auth/pkg/response"
	"github.com/gin-gonic/gin"
)

// get
//   获取单个的 Userroles_model
func GetOneUSerroles(c *gin.Context) {
	var UserrolesQuery model.Userroles_model
	UserrolesQuery.UserName = c.Query("name")
	if UserrolesQuery.UserName =="" {
		response.GInFailed(c, "name不能为空")
		return
	}
	data, err := data_source_crd.Userroles_service.Get(UserrolesQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, data)
}

// getlist
// 获取RoleGrant 列表
func GetListSerroles(c *gin.Context) {
	var UserrolesQuery model.Userroles_model
	//err := c.BindJSON(&UserrolesQuery)
	//if err != nil {
	//	response.GInFailed(c, "输入参数不正确")
	//}
	data, err := data_source_crd.Userroles_service.GetList(UserrolesQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, data)
}

// post
// 创建一个 RoleGrant
func CreateSerrole(c *gin.Context) {
	var UserrolesQuery model.Userroles_model
	err := c.BindJSON(&UserrolesQuery)
	if err != nil {
		response.GInFailed(c, "输入参数不正确")
		return
	}
	err = data_source_crd.Userroles_service.Post(UserrolesQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "创建成功")
}

// put
// 更新 RoleGrant
func PutListSerrole(c *gin.Context) {
	var UserrolesQuery model.Userroles_model
	err := c.BindJSON(&UserrolesQuery)
	if err != nil {
		response.GInFailed(c, "输入参数不正确")
		return
	}
	err = data_source_crd.Userroles_service.Put(UserrolesQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "修改成功")
}

// delete
// 删除 RoleGrant
func DeleteListSerrole(c *gin.Context) {
	var UserrolesQuery model.Userroles_model
	UserrolesQuery.UserName = c.Query("name")
	if UserrolesQuery.UserName =="" {
		response.GInFailed(c, "name不能为空")
		return
	}
	err := data_source_crd.Userroles_service.Delete(UserrolesQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "删除成功")
}
