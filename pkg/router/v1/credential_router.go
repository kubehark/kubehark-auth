package v1

import (
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd"
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd/model"
	"gitee/kubehark/kubehark-auth/pkg/response"
	"github.com/gin-gonic/gin"
)

// get
//   获取单个的Credntial
func GetOneCredntial(c *gin.Context) {
	var CredentialQuery model.Credential_model
	CredentialQuery.Name = c.Query("name")
	if CredentialQuery.Name =="" {
		response.GInFailed(c, "name不能为空")
		return
	}
	data, err := data_source_crd.Credential_service.Get(CredentialQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, data)
}

// getlist
// 获取Credntial 列表
func GetListCredntial(c *gin.Context) {
	var CredentialQuery model.Credential_model
	//err := c.BindJSON(&CredentialQuery)
	//if err != nil {
	//	response.GInFailed(c, "输入参数不正确")
	//}
	data, err := data_source_crd.Credential_service.GetList(CredentialQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, data)
}

// post
// 创建一个Credntial
func CreateCredntial(c *gin.Context) {
	var CredentialQuery model.Credential_model
	err := c.BindJSON(&CredentialQuery)
	if err != nil {
		response.GInFailed(c, "输入参数不正确")
		return
	}
	err = data_source_crd.Credential_service.Post(CredentialQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "创建成功")
}

// put
// 更新Credntial
func PutListCredntial(c *gin.Context) {
	var CredentialQuery model.Credential_model
	err := c.BindJSON(&CredentialQuery)
	if err != nil {
		response.GInFailed(c, "输入参数不正确")
	}
	err = data_source_crd.Credential_service.Put(CredentialQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "修改成功")
}

// delete
// 删除Credntial
func DeleteListCredntial(c *gin.Context) {
	var CredentialQuery model.Credential_model
	CredentialQuery.Name = c.Query("name")
	if CredentialQuery.Name =="" {
		response.GInFailed(c, "name不能为空")
		return
	}
	err := data_source_crd.Credential_service.Delete(CredentialQuery)
	if err != nil {
		response.GInFailed(c, err.Error())
		return
	}
	response.GinSuccess(c, "删除成功")
}
