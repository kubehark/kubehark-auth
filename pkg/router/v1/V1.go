package v1

import (
	"gitee/kubehark/kubehark-auth/pkg/middlewares/cors"
	"github.com/gin-gonic/gin"
)

var (
	RegoModelPath = []string{}
)

func V1(router *gin.Engine, regoPath []string) {
	RegoModelPath = regoPath
	router.Use(cors.Cors())
	sr := router.Group("/v1")
	{
		credentialRouter := sr.Group("/credentials")
		{
			credentialRouter.GET("/get", GetOneCredntial)
			credentialRouter.GET("/getlist", GetListCredntial)
			credentialRouter.POST("/post", CreateCredntial)
			credentialRouter.PUT("/put", PutListCredntial)
			credentialRouter.DELETE("/delete", DeleteListCredntial)
		}

		roleGrantRouter := sr.Group("/rolegrant")
		{
			roleGrantRouter.GET("/get", GetOneRoleGrant)
			roleGrantRouter.GET("/getlist", GetListRoleGrant)
			roleGrantRouter.POST("/post", CreateRoleGrant)
			roleGrantRouter.PUT("/put", PutListRoleGrant)
			roleGrantRouter.DELETE("/delete", DeleteListRoleGrant)
		}

		userrolesRouter := sr.Group("/userroles")
		{
			userrolesRouter.GET("/get", GetOneUSerroles)
			userrolesRouter.GET("/getlist", GetListSerroles)
			userrolesRouter.POST("/post", CreateSerrole)
			userrolesRouter.PUT("/put", PutListSerrole)
			userrolesRouter.DELETE("/delete", DeleteListSerrole)
		}

		oauthRouter := sr.Group("/oauth")
		{
			oauthRouter.POST("/", OauthResult)
		}

	}
}
