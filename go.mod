module gitee/kubehark/kubehark-auth

go 1.16

require (
	gitee/kubehark/kubehark-operator v0.0.0
	github.com/coreos/go-oidc/v3 v3.1.0
	github.com/gin-gonic/gin v1.7.4
	github.com/open-policy-agent/opa v0.33.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	github.com/spf13/pflag v1.0.5
	golang.org/x/oauth2 v0.0.0-20210514164344-f6687ab2804c
	k8s.io/apimachinery v0.22.2
	k8s.io/client-go v0.22.2
)

replace gitee/kubehark/kubehark-operator => ../kubehark-operator
