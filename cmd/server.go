package cmd

import (
	"fmt"
	"gitee/kubehark/kubehark-auth/pkg/data_source_crd"
	router "gitee/kubehark/kubehark-auth/pkg/router"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"io"
)

var serverLong = "kubehark oidc 服务"

var ServerCmd serverCmd
type serverCmd struct {
	out           io.Writer
	kubeconfig    string
	RegoModelPath []string
}

func NewServerCmd(out io.Writer) *cobra.Command {
	ServerCmd = serverCmd{out: out,RegoModelPath: []string{}}
	var cmd = &cobra.Command{
		Use:   "server",
		Short: "kubehark oidc server",
		Long:  serverLong,
		Run: func(c *cobra.Command, args []string) {
			//fmt.Println("server")
			//fmt.Println(args)


			fmt.Println(ServerCmd.kubeconfig)
			fmt.Println(ServerCmd.RegoModelPath)
			// 初始化client
			data_source_crd.Init(ServerCmd.kubeconfig)
			ginRouter := router.InitRootRouter(ServerCmd.RegoModelPath)
			ginRouter.Run(":8081")
		},
	}
	fs := cmd.PersistentFlags()
	ServerCmd.AddFlags(fs)
	return cmd
}

func (this *serverCmd) AddFlags(fs *pflag.FlagSet) {
	fs.StringVarP(&this.kubeconfig, "kubeconfig", "k", "", "kubeconfig config")
	fs.StringArrayVarP(&this.RegoModelPath, "rego-model-path", "r", []string{}, "rego models path")
}
