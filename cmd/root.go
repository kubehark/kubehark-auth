package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)


var RootLong = "kubehark oidc 服务"

func NewRootCmd(args []string) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "kubehark",
		Short: "kubehark oidc server",
		Long:  RootLong,
		Run: func(c *cobra.Command, args []string) {
			fmt.Println("root")
			fmt.Println(args)
		},
	}
	flags := cmd.PersistentFlags()
	flags.Parse(args)
	out := cmd.OutOrStdout()
	cmd.AddCommand(
		NewServerCmd(out),
		)
	return cmd

}

