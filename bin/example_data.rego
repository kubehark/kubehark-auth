package data

default permission_labels = ["pl1","pl2"]

default deny_rule_labels = [
#    {
#       "labels":{
#            "dl1":["dlv1"]
#       },
#       # true 表示只要含有一个标签就生效  false表示必须所有labels匹配到了才生效
#       "any" : true,
#       "method":["GET","POST","PUT","PATCH","DELETE"]
#    },
#    {
#        "labels":{
#            "dl1":["dlv1"],
#            "dl2":["dlv2"]
#        },
#        # true 表示只要含有一个标签就生效  false表示必须所有labels匹配到了才生效
#        "any" : true,
#        "method":["POST","PUT","PATCH","DELETE"]
#    }
]
user_roles= [
          {
             "user_name" : "test_user",
             "roles" : ["view"],
             "white_list" : ["111"]
          },
          {
               "user_name" : "admin",
               "roles" : ["view"],
               "white_list" : ["111"]
          }
 ]
role_grants= [
   {
      "name" : "view",
      "method" : ["GET"],
      "labels" : {
          "A": "A"
      }
   },
   {
         "name" : "2",
         "method" : ["*"],
         "labels" : {
             "A": "*"
         }
      }

]

