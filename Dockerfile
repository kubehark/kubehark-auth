FROM ubuntu
WORKDIR /
COPY   ./bin  /
USER 8081:8081
ENTRYPOINT ["/manager"]